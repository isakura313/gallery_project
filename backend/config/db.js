const mongoose = require("mongoose"); // Mongoose представляет специальную ODM-библиотеку (Object Data Modelling) для работы с MongoDB, которая позволяет сопоставлять объекты классов и документы коллекций из базы данных.
const MONGOURI = "mongodb://mongo:27017/"; // uncomment this for docker

const InitiateMongoServer = async () => {
  try {
    await mongoose.connect(MONGOURI, {
      useNewUrlParser: true,
    });
    console.log("Произошло подключение к базе данных");
  } catch (e) {
    console.log(e, "произошла ошибка подключения");
    throw e;
  }
};

module.exports = InitiateMongoServer;
