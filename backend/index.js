const express = require("express");
const bodyParser = require("body-parser");
const user = require("./routes/user");
const file = require("./routes/file");
const InitiateMongoServer = require("./config/db");

InitiateMongoServer(); // Берем Монго сервер

const app = express();

// PORT
const PORT = process.env.PORT || 80;

// Middleware
app.use(bodyParser.json());
app.get("/api", (req, res) => {
  res.json({ message: "API запущен" });
});
app.use("/api/public", express.static("public")); //раздаем статику  - в первую очередь это будут загружаемый файлы
app.use("/api/files", file); /// раздаем файлы

app.use("/api/user", user);

app.listen(PORT, (req, res) => {
  console.log(`Server Started at PORT ${PORT}`);
});
